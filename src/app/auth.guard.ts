import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ValidarService } from './services/validar.service';

//

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private validarServices: ValidarService,
    private router: Router
   ) { }

  canActivate(): boolean {
    if (this.validarServices.loggedIn()) {
      return true;
    }
    this.router.navigate(['/inicio']);
    return false;
  }

}
