import { TestBed } from '@angular/core/testing';

import { AcuerdoFundacionService } from './acuerdo-fundacion.service';

describe('AcuerdoFundacionService', () => {
  let service: AcuerdoFundacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AcuerdoFundacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
