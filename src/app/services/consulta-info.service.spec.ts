import { TestBed } from '@angular/core/testing';

import { ConsultaInfoService } from './consulta-info.service';

describe('ConsultaInfoService', () => {
  let service: ConsultaInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConsultaInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
