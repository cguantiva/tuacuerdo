import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Acuerdo} from '../interfacesFundacion/acuerdo';
import {Interaccion} from '../interfacesFundacion/interacciones';

@Injectable({
  providedIn: 'root'
})
export class AcuerdoFundacionService {

  data: any = {};
  private url = 'http://34.74.133.198:8084/api/';

  constructor(private http: HttpClient) { }

  GuardarAcuerdo(acuerdo: Acuerdo) {
  
    console.log("Acuerdo: ", acuerdo);
    
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      // tslint:disable-next-line: max-line-length
     
    });
    return this.http.post(`${this.url}acuerdos/save`, acuerdo, {headers});
  }

  GuardarInteracciones(interaccion: Interaccion){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      // tslint:disable-next-line: max-line-length
     
    });
    return this.http.post(`${this.url}interacciones`, interaccion, {headers});
  }

  uploadFile(formData, token) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
       token: token
      // tslint:disable-next-line: max-line-length
     
    });
    return this.http.post(`${this.url}uploads`, formData);
  }

  
}
