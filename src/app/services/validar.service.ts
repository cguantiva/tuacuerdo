import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ValidarService {

  constructor(private router: Router) { }

  loggedIn() {
    if (localStorage.getItem('token')) {
      return true;
    }
    else {
      return false;
    }
  }

  loggedOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('negociacion_agrupada');
    localStorage.removeItem('nombreLog');
    this.router.navigate(['/inicio']);
  }
}
