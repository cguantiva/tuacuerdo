import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ConsultaInfoService {

  private url = 'http://34.74.133.198:8084/api/ofertas/';

  constructor(private http: HttpClient,
    private router: Router) { }

    infoInicial(userData) {
      const url = `${this.url}tmto/${userData}`
      console.log("URL:",url);
      return this.http.get(url); 
    }

    ofertas(token: any, opt: any, docuUsu: any) {
      const url = `${this.url}${token}/${opt}/${docuUsu}`
      console.log("URL:",url);
      return this.http.get(url); 
    }
}
