import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { BsDatepickerConfig, BsDaterangepickerDirective } from 'ngx-bootstrap/datepicker';
import { ConsultaInfoService } from 'src/app/services/consulta-info.service';
import { AcuerdoFundacionService } from 'src/app/services/acuerdo-fundacion.service';
import {Acuerdo} from '../../interfacesFundacion/acuerdo';
import {Interaccion} from '../../interfacesFundacion/interacciones';
import Swal from 'sweetalert2';
import 'sweetalert2/src/sweetalert2.scss';

@Component({
  selector: 'app-funda-mujer',
  templateUrl: './funda-mujer.component.html',
  styleUrls: ['./funda-mujer.component.css']
})
export class FundaMujerComponent implements OnInit {


  docu = null;
  docuLog: any;
  docuMostrar: any;
  nomLog: string;
  fundaCredito: any;
  valorTotalMora: any;
  diasMora: any;
  pagoTotal: any;
  pagoTotalResumen: any;
  descuento: any;
  medPago: any;
  flagBtnEnvFot: boolean = false;
  flagBtnCargando: boolean = false;;
  nombImgSoporte: any;
  canOfertas:any;
  idOferta: any;
  fechaPago: any;
  numCuotas: any;
  ofertasIndividuales: any = {};
  flag1: any;

  

  contador = 0;
  intentos = 3;
  Fecha: string;
  FechaMoment: any;
  FechaMoment2: any;
  fechaFinal: any;
  myDateValueI: Date;
  myDateValueCuo: Date;
  cantCuotas: any;
  valCuotAprox: any;
  valCuotAproxMostrar: any;
  valTotalMora: any;
  motivNoPag: any = '0';
  minDate: Date;
  maxDate: Date;
  token: string;
  negociacion_agrupada: any;
  data: any = {};
  oferta: any = {};

  acuerdo: Acuerdo = {
    idOferta: '',
    valor_acuerdo: '',
    valor_cuota : '',
    fecha_pago: '',
    numero_cuotas: '',
    token: '',
    punto_pago: ''
  }

  interaccion: Interaccion = {
    idResultado: '',
    nota: '',
    token: ''
  }

  interaccionEvento: Interaccion = {
    idResultado: '',
    nota: '',
    token: ''
  }

  uploadedFiles: Array < File > ;

  constructor(private consultaInfoService: ConsultaInfoService, private acuerdoFundacionService: AcuerdoFundacionService) { 
    this.minDate = new Date();
    this.maxDate = new Date();
    this.minDate.setDate(this.minDate.getDate() +1);
    this.maxDate.setDate(this.maxDate.getDate() + 6);
  }

  ngOnInit(): void {
    this.token = localStorage.getItem('token');
    this.negociacion_agrupada = localStorage.getItem('negociacion_agrupada');
    this.nomLog = localStorage.getItem('nombreLog');
    this.docuLog = localStorage.getItem('docuLog');
    this.docuMostrar =  this.docuLog.substr(-4);
    //alert(this.negociacion_agrupada)

    if(document.getElementById("btnModal")){
      var modal = document.getElementById("tvesModal");
      var btn = document.getElementById("btnModal");
      var span = document.getElementsByClassName("close")[0];
      var body = document.getElementsByTagName("body")[0];

      btn.onclick = function() {
        modal.style.display = "block";

        body.style.position = "static";
        body.style.height = "100%";
        body.style.overflow = "hidden";
      }

      window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none";

          body.style.position = "inherit";
          body.style.height = "auto";
          body.style.overflow = "visible";
        }
      }
    }
  }

  onValueChange(value: Date): void {
    this.myDateValueI = value;
    if(value != undefined){
     this.FechaMoment = moment(value).format("YYYY-MM-DD");
     this.fechaPago = this.FechaMoment;
    }
    
  }

  onValueChange2(value: Date): void {
    this.myDateValueCuo = value;
    if(value != undefined){
    this.FechaMoment2 = moment(value).format("YYYY-MM-DD");
    this.fechaPago = this.FechaMoment2;
    }
  }

  cerrarModal(){
    if(document.getElementById("btnModal")){
      var modal2 = document.getElementById('tvesModal');
      var body1 = document.getElementsByTagName("body")[0];

      
      modal2.style.display = "none";
      body1.style.position = "inherit";
      body1.style.height = "auto";
      body1.style.overflow = "visible";    
    }
  }

  validar(){
    if(this.docu == null){
      Swal.fire({
        icon: 'warning',
        title: '¡Oopss!',
        text: 'Debe ingresar un número de documento'
      })
    }else{

      if(this.docu != this.docuLog){
        this.interaClick('10209','errVal');
        this.contador = this.contador + 1;
        if(this.intentos > 1 && this.contador != 3){
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'El usuario es erroneo, le quedan: ' + (this.intentos = this.intentos - 1) + ' intentos'
          });
        }
        else{
          Swal.fire({
            icon: 'warning',
            title: '¡Falló la autenticación!',
            text: 'Favor comunicarse al número de Whatsapp ###'
          })
        }
        if(this.contador == 3){
          Swal.fire({
            icon: 'warning',
            title: '¡Falló la autenticación!',
            text: 'Favor comunicarse al número de Whatsapp ###'
          })
        }
      }else{
        this.interaClick('10209','exitoVal');
       // console.log("token: ", this.token, "neg: ", this.negociacion_agrupada, "docu: ", this.docu);
        this.consultaInfoService.ofertas(this.token, this.negociacion_agrupada, this.docu ).subscribe(
          res =>{
            console.log(res);
            this.data = res;
            console.log(" Respuesta ofertas: ", this.data.oferta);
            this.canOfertas = this.data.canOfertas;
            if(this.data.ofertas == '' || this.data.oferta == ''){
  
            }else{
              this.oferta = this.data.oferta;
              for(let i = 0; i < this.oferta.numero_producto.length; i++){
                this.fundaCredito = this.oferta.numero_producto;
              }
              this.idOferta = this.oferta.idOferta;
              this.valorTotalMora = this.oferta.valor_original_saldo_total;
              this.diasMora = this.oferta.dias_mora;
              this.pagoTotal = this.oferta.valor_original_saldo_total;
              this.descuento = this.oferta.descuento_general;
              this.numCuotas = this.oferta.numero_cuotas;
              if( this.numCuotas > 1){
                this.mostrarOcultar('divUnBoton','divdosBotones');
              }else{
                this.mostrarOcultar('divdosBotones','divUnBoton');
              }
              this.mostrarOcultar('validar', 'inicio');
            }
          },
          err =>{
            console.log(err);
            alert('No hay respuesta del servidor');
          });
      }
  }
  };

  consultaIncial(){
    this.flag1 = 0;
    this.consultaInfoService.ofertas(this.token, '1', this.docu ).subscribe(
      res =>{
        console.log(res);
        this.data = res;
        console.log(" Respuesta ofertas: ", this.data.oferta);
        this.canOfertas = this.data.canOfertas;
        if(this.data.ofertas == '' || this.data.oferta == ''){

        }else{
          this.oferta = this.data.oferta;
          for(let i = 0; i < this.oferta.numero_producto.length; i++){
            this.fundaCredito = this.oferta.numero_producto;
          }
          this.idOferta = this.oferta.idOferta;
          this.valorTotalMora = this.oferta.valor_original_saldo_total;
          this.diasMora = this.oferta.dias_mora;
          this.pagoTotal = this.oferta.valor_original_saldo_total;
          this.descuento = this.oferta.descuento_general;
          this.numCuotas = this.oferta.numero_cuotas;
          if( this.numCuotas > 1){
            this.mostrarOcultar('divUnBoton','divdosBotones');
          }else{
            this.mostrarOcultar('divdosBotones','divUnBoton');
          }
          this.mostrarOcultar('indiviFund', 'estadoCuenta');
        }
      },
      err =>{
        console.log(err);
        alert('No hay respuesta del servidor');
      });
  }

  mostrarOcultar(ocultar, mostrar){
    
    var divOcultar = document.getElementById(ocultar);
    var divMostrar = document.getElementById(mostrar);
    var footer = document.getElementById('footer');
    divOcultar.style.display = "none";
    divMostrar.style.display = "block";

    if(mostrar == 'estadoCuenta'){
      this.interaClick('10101  ','ingreEstCuenta');
    }
    if(mostrar == 'resumen'){
      this.interaClick('10203 ','ingreHome');
    }
    if(ocultar == 'inicio'){
       footer.style.display = "block"
    }
    this.validarCuotas(mostrar);

  }

  validarCuotas(mostrar: any){
    if(mostrar == 'estadoCuenta'){
      
    }else if(mostrar == 'pagoCuotas'){
      this.numCuotas = this.cantCuotas;
    }
    if(mostrar == 'resumen'){
      if(this.numCuotas > 1){
        this.fechaFinal = this.FechaMoment2;
      }else{
        this.fechaFinal = this.FechaMoment;
      }
    }
  }


  operaCuotas(){
    console.log(this.cantCuotas);
    console.log(this.valCuotAprox);
    if(this.cantCuotas != undefined){
      this.numCuotas = this.cantCuotas;
      this.valCuotAprox = this.valorTotalMora/this.cantCuotas
      this.valCuotAproxMostrar = Math.round(this.valCuotAprox);
      this.pagoTotal = this.valCuotAprox;
      this.interaClick('10104 ','operCouta');
    }

    
    
  }

  envMotNoPag(){
    Swal.fire({
      icon: 'success',
      title: 'NOTIFICACIÓN',
      text: 'Hemos recibido su motivo de no pago. Pronto estaremos en contacto nuevamente',
      showDenyButton: false,
      showCancelButton: false,
      confirmButtonText: `Finalizar`,
      confirmButtonColor: '#872174'
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
      this.interaClick('10103','motNopag');
      this.mostrarOcultar('motivoNoPago','verOferta');
  
      //	window.location = "./verOferta.html";
        //Swal.fire('Saved!', '', 'success')
        
      }
    })
  }

  medioPago(medio: any){

    if(medio == '1'){
      medio = 'PSE';
    }else if(medio == '2'){
      medio = 'App Fundación delamujer';
    }else if(medio == '3'){
      medio = 'Oficina morada Fundación delamujer';
    }else if(medio == '4'){
      medio = 'Efecty';
    }else if(medio == '5'){
      medio = 'Baloto';
    }
    this.medPago = medio;

    Swal.fire({
      icon: 'success',
      title: medio,
      text: 'Seleccionó '+medio+' como medio de pago.',
      showDenyButton: false,
      showCancelButton: true,
      confirmButtonText: `Continuar`,
      confirmButtonColor: '#872174'
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
  
      this.guargarAcuerdo(medio);
      //	window.location = "./verOferta.html";
        //Swal.fire('Saved!', '', 'success')
        
      }
    })
  }

  guargarAcuerdo(medioPago: any){
    this.acuerdo.idOferta =  this.idOferta;
    this.acuerdo.valor_acuerdo = this.pagoTotal;
    this.acuerdo.valor_cuota = this.pagoTotal;
    this.acuerdo.fecha_pago = this.fechaPago;
    this.acuerdo.numero_cuotas = this.numCuotas;
    this.acuerdo.token = this.token;
    this.acuerdo.punto_pago = medioPago;

    console.log("datos de envío: ", this.acuerdo);
    this.acuerdoFundacionService.GuardarAcuerdo(this.acuerdo).subscribe(
      res => {
      //  console.log(res);

        Swal.fire({
          icon: 'success',
          title: 'Acuerdo realizado',
          allowOutsideClick: false,
          text: 'Nuestro sistema se encuentra registrando su compromiso de pago, una vez se ejecute el proceso recibirá una llamada con la confirmación , para que pueda efectuar su pago en el lugar y la fecha indicada. ',
          showDenyButton: false,
          showCancelButton: false,
          confirmButtonText: `Continuar`,
          confirmButtonColor: '#872174'
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            this.cerrarModal();
            this.mostrarOcultar('terminosCondiciones','resumen');
          }
        })
        
      },
      err =>{
        console.log(err);
      }
    )
    

  }
  
  info(){
    Swal.fire(
      'Fundamujer',
      'Información para lograr hacer un acuerdo de pago de sus obligaciones',
      'info',     
    )
  }

  abrirModalPago(){
  
    var modal1 = document.getElementById('comPago');
    var modal2 = document.getElementById('tvesModal');

    var span = document.getElementsByClassName("close")[0];
    var body1 = document.getElementsByTagName("body")[0];

    
      modal1.style.display = "block";
      body1.style.position = "static";
      body1.style.height = "100%";
      body1.style.overflow = "hidden";
    

    window.onclick = function(event) {
      if (event.target == modal1 || event.target == modal2) {
        modal1.style.display = "none";
        modal2.style.display = "none";

        body1.style.position = "inherit";
        body1.style.height = "auto";
        body1.style.overflow = "visible";
      }
    }
  
}



// funciones para interacciones

 interaClick(idResultado: any, evento: any){
  this.interaccion.token = this.token;
  this.interaccionEvento.token = this.token;

  if(evento == 'btn1'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'click en boton ingresar en vista de validación de usuario';
    this.guardarInteraccion(this.interaccion);
  }
  if(evento == 'btn2'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'click en boton si en vista de informacion de alternativa';
     this.guardarInteraccion(this.interaccion);
  }
  if(evento == 'btn3'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'click en boton no en vista de informacion de alternativa';
    this.guardarInteraccion(this.interaccion);
  }
  if(evento == 'btn4'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'cliente solicita ver oferta en vista de motivo de no pago en gestion';
    this.guardarInteraccion(this.interaccion);
  }
  if(evento == 'btn5'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'cliente solicita ver pago de contado';
    this.guardarInteraccion(this.interaccion);
  }
  if(evento == 'btn6'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'cliente solicita ver pago a cuotas';
    this.guardarInteraccion(this.interaccion);
  }

  if(evento == 'btn7'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'cliente solicita ver fundacreditos individuales';
    this.guardarInteraccion(this.interaccion);
  }
  if(evento == 'btn8'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'click en boton no me intereza';
    this.mostrarOcultar('estadoCuenta','motivoNoPago');
    this.guardarInteraccion(this.interaccion);
  }


  if(evento == 'btn9'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'cliente solita regresar a la vista home';
    this.guardarInteraccion(this.interaccion);
  }
  if(evento == 'btn10'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'cliente confirma valor del pago total';
    this.guardarInteraccion(this.interaccion);
  }
  if(evento == 'btn11'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'click en boton regresar a la vista home';
    this.guardarInteraccion(this.interaccion);
  }
  if(evento == 'btn12'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'click confirmar valor aproximado de cuota en pago a coutas';
    this.guardarInteraccion(this.interaccion);
  }
  if(evento == 'btn13'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'click en regresar a la vista home desde vista terminos y condiciones';
    this.guardarInteraccion(this.interaccion);
  }
  if(evento == 'btn14'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'click en aceptar terminos y condiciones';
    this.guardarInteraccion(this.interaccion);
  }
  if(evento == 'btnwt'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'click en boton whatsapp';
    this.guardarInteraccion(this.interaccion);
  }
  if(evento == 'btninfo'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'click en boton informacion';
    this.guardarInteraccion(this.interaccion);
  }
  if(evento == 'btnpago'){
    this.interaccion.idResultado = idResultado;
    this.interaccion.nota = 'click en boton ya pagué';
    this.guardarInteraccion(this.interaccion);
  }

  



  /**********/
  if(evento == 'errVal'){
    this.interaccionEvento.idResultado = idResultado;
    this.interaccionEvento.nota = 'Verifiación de usuario no exitosa';
    this.guardarInteraccion(this.interaccionEvento);
  }
  if(evento == 'exitoVal'){
    this.interaccionEvento.idResultado = idResultado;
    this.interaccionEvento.nota = 'Verifiación de usuario exitosa';
    this.guardarInteraccion(this.interaccionEvento);
  }
  if(evento == 'motNopag'){
    this.interaccionEvento.idResultado = idResultado;
    this.interaccionEvento.nota = 'motivo: '+this.motivNoPag + ', al seleccionar: no conocer alternativa ';
    this.guardarInteraccion(this.interaccionEvento);
  }
  if(evento == 'ingreHome'){
    this.interaccionEvento.idResultado = idResultado;
    this.interaccionEvento.nota = 'Ingreso al home';
    this.guardarInteraccion(this.interaccionEvento);
  }
  if(evento == 'ingreEstCuenta'){
    this.interaccionEvento.idResultado = idResultado;
    this.interaccionEvento.nota = 'consultar oferta';
    this.guardarInteraccion(this.interaccionEvento);
  }
  if(evento == 'operCouta'){
    this.interaccionEvento.idResultado = idResultado;
    this.interaccionEvento.nota = 'El cliente seleccino: '+ this.cantCuotas + ', el valor aproximado de la cuota es:' +this.valCuotAprox;
    this.guardarInteraccion(this.interaccionEvento);
  }

  if(evento == 'wht'){
    this.interaccionEvento.idResultado = idResultado;
    this.interaccionEvento.nota = 'cliente se comunica por whatsaap';
    this.guardarInteraccion(this.interaccionEvento);
  }
  

  
  
}
async guardarInteraccion(interaccion: Interaccion){
  
 await console.log("guardar interacción: ", interaccion);
 /*await this.acuerdoFundacionService.GuardarInteracciones(interaccion).subscribe(
   res =>{
    console.log(res);
   },
   err =>{
     console.log(err);
     
   }
 )*/
}

verFundacred(){
  this.mostrarOcultar('estadoCuenta','indiviFund');
  this.negociacion_agrupada = 0;
  this.consultaInfoService.ofertas(this.token, this.negociacion_agrupada, this.docu ).subscribe(
    res =>{
      console.log(res);
      this.data = res;
      console.log(" Respuesta ofertas: ", this.data.ofertas);
      this.canOfertas = this.data.canOfertas;
      if(this.data.ofertas == '' || this.data.oferta == ''){
      }else{
        this.oferta = this.data.ofertas;
        for(let i = 0; i < this.oferta.length; i++){
        }
        if( this.numCuotas > 1){
          this.mostrarOcultar('divUnBoton','divdosBotones');
        }else{
          this.mostrarOcultar('divdosBotones','divUnBoton');
        }
      }
    },
    err =>{
      console.log(err);
      alert('No hay respuesta del servidor');
    });
}

verFundInd(idFunda: any){
  this.negociacion_agrupada = 0;
  this.consultaInfoService.ofertas(this.token, this.negociacion_agrupada, this.docu ).subscribe(
    res =>{
      //console.log(res);
      this.data = res;
     // console.log(" Respuesta ofertas: ", this.data.ofertas);
      this.canOfertas = this.data.canOfertas;
      if(this.data.ofertas == '' || this.data.oferta == ''){

      }else{
        this.oferta = this.data.ofertas;
        for(let i = 0; i < this.oferta.length; i++){
          if(this.oferta[i].id == idFunda){
            this.ofertasIndividuales = this.oferta[i];
          }
        }
        this.idOferta = this.ofertasIndividuales.id;
        this.valorTotalMora = this.ofertasIndividuales.valor_original_saldo_total;
        this.diasMora = this.ofertasIndividuales.dias_mora;
        this.pagoTotal = this.ofertasIndividuales.valor_original_saldo_total;
        this.descuento = this.ofertasIndividuales.descuento_general;
        this.numCuotas = this.ofertasIndividuales.numero_cuotas;
        this.fundaCredito = this.ofertasIndividuales.numero_producto;
        this.flag1 = 1;
        this.mostrarOcultar('indiviFund','estadoCuenta');
      }
    },
    err =>{
      console.log(err);
      alert('No hay respuesta del servidor');
    });
}


fileChange(element) {
  this.flagBtnCargando = true;
  if(element){
    this.flagBtnCargando = false;
    this.flagBtnEnvFot = true;
  }
  this.uploadedFiles = element.target.files;
}

upload() {
  let formData = new FormData();
  for (var i = 0; i < this.uploadedFiles.length; i++) {
    formData.append("uploads[]", this.uploadedFiles[i], this.uploadedFiles[i].name);
  }
  this.acuerdoFundacionService.uploadFile(formData, this.token).subscribe((res)=> {
    console.log('response received is ', res);
    Swal.fire({
      icon: 'success',
      title: 'Documento enviado con éxito',
      allowOutsideClick: false,
      text: 'Muchas gracias',
      showDenyButton: false,
      showCancelButton: false,
      confirmButtonText: `Continuar`,
      confirmButtonColor: '#872174'
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        window.location.reload();
      }
    })

  },
  err => {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Hubo un error al cargar el archivo. Intente de nuevo'
    })
    console.log(err);
    
  });
  }

}
