import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundaMujerComponent } from './funda-mujer.component';

describe('FundaMujerComponent', () => {
  let component: FundaMujerComponent;
  let fixture: ComponentFixture<FundaMujerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FundaMujerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundaMujerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
