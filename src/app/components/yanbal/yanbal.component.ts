import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { $ } from 'protractor';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ConsultaInfoService } from 'src/app/services/consulta-info.service';

@Component({
  selector: 'app-yanbal',
  templateUrl: './yanbal.component.html',
  styleUrls: ['./yanbal.component.css']
})
export class YanbalComponent implements OnInit {
  Fecha: string;
  FechaMoment: string;
  myDateValueI: Date;

  flagBtnEnvFot: boolean = false;
  nombImgSoporte: any;
  datos: any [];

  valPago: any;
  valFecha: any;
  valFaltante: any;

  totalDeuda: any;
  valRadPorcen: any;
  valRadFecha: any;
  fecha1: any = 'Viernes 26/11/2020';
  fecha2: any = 'Sabado 27/11/2020';
  fecha3: any = 'Domingo 28/11/2020';

  token: any;
  data: any = {};
  nomUsu: any;
  numPedido: any;

  constructor(private consultaInfoService: ConsultaInfoService) { }


  form = new FormGroup({
    rbPorc: new FormControl('', Validators.required)
  });

  form2 = new FormGroup({
    rbFech: new FormControl('', Validators.required)
  });
  
  get e(){
    return this.form.controls;
  }
  
  submit(){
    this.valRadPorcen = this.form.value.rbPorc;
    this.valRadFecha = this.form2.value.rbFech;
    //console.log(this.form.value.rbPorc);
    //console.log(this.form2.value.rbFech);
    this.operaCuotas();

  }
  

  ngOnInit(): void {

    this.token = localStorage.getItem('token');

    this.consultaInfoService.infoInicial(this.token).subscribe(
      res =>{
        console.log("completo: ",res);
        this.data = res;
        this.datos = this.data.resulOfertas;
        this.nomUsu = this.datos[0].nombres_persona;
        //console.log("saldoTotal: ", this.valSaldoTotal );
      },
      err =>{
        console.log(err);
        
      }
    )

  }

  abrirModal(valor: any, numPedido: any){
      this.totalDeuda = valor;
      this.numPedido = numPedido;
      var modal1 = document.getElementById('comPago');
      var modal2 = document.getElementById('tvesModal');
      var btn1 = document.getElementById('btnModalPago');
      var btn2 = document.getElementById('btnModal');

      var span = document.getElementsByClassName("close")[0];
      var body1 = document.getElementsByTagName("body")[0];

      btn1.onclick = function() {
        modal1.style.display = "block";
        body1.style.position = "static";
        body1.style.height = "100%";
        body1.style.overflow = "hidden";
      }

     
        modal2.style.display = "block";
        body1.style.position = "static";
        body1.style.height = "100%";
        body1.style.overflow = "hidden";
      

      window.onclick = function(event) {
        if (event.target == modal1 || event.target == modal2) {
          modal1.style.display = "none";
          modal2.style.display = "none";

          body1.style.position = "inherit";
          body1.style.height = "auto";
          body1.style.overflow = "visible";
        }
      }
    
  }

  abrirModalPago(){
  
    var modal1 = document.getElementById('comPago');
    var modal2 = document.getElementById('tvesModal');

    var span = document.getElementsByClassName("close")[0];
    var body1 = document.getElementsByTagName("body")[0];

    
      modal1.style.display = "block";
      body1.style.position = "static";
      body1.style.height = "100%";
      body1.style.overflow = "hidden";
    

    window.onclick = function(event) {
      if (event.target == modal1 || event.target == modal2) {
        modal1.style.display = "none";
        modal2.style.display = "none";

        body1.style.position = "inherit";
        body1.style.height = "auto";
        body1.style.overflow = "visible";
      }
    }
  
}



  validarModal(id: any, modal: any){
    console.log("id: ", id + " modal: ", modal);
    
  }

  filtrarFecha(){
    this.FechaMoment = moment(this.Fecha).format("YYYY-MM-DD");

    console.log("FechaMoment: ",this.FechaMoment);   
    
  }

  public cargandoImagen(files: FileList){
    if(files){
     this.flagBtnEnvFot = true;
    }

    const formData: FormData = new FormData();

    console.log("image: ", files[0]);
    this.nombImgSoporte =  files[0].name;
    console.log("Nombre imagen: ", this.nombImgSoporte);
  }




  cerrarModal($event){
    $event.preventDefault();
    if(document.getElementById("btnModal")){
      var modal1 = document.getElementById('comPago');
      var modal2 = document.getElementById('tvesModal');
      var span = document.getElementsByClassName("close")[0];
      var body1 = document.getElementsByTagName("body")[0];

      modal1.style.display = "none";
      modal2.style.display = "none";

      body1.style.position = "inherit";
      body1.style.height = "auto";
      body1.style.overflow = "visible";    
    }
  }

  cerrarModal2(){
    if(document.getElementById("btnModal")){
      var modal1 = document.getElementById('comPago');
      var modal2 = document.getElementById('tvesModal');
      var span = document.getElementsByClassName("close")[0];
      var body1 = document.getElementsByTagName("body")[0];

      modal1.style.display = "none";
      modal2.style.display = "none";

      body1.style.position = "inherit";
      body1.style.height = "auto";
      body1.style.overflow = "visible";    
    }
  }



  mostrarOcultar(ocultar, mostrar){
    var divOcultar = document.getElementById(ocultar);
    var divMostrar = document.getElementById(mostrar);
  
    divOcultar.style.display = "none";
    divMostrar.style.display = "block";
  }

  /* función para crear dinámicamente los radioButtons de fecha


  asignarFecha(){	 
    var id = 'fecha';
    var contador = 0;
 
    for(let i = 0; i < this.arregloFechPag.length; i++) { 
       var contador = contador + 1;
       var id2 = id + contador;
       var divN = 'divN'+ contador;
       var newDiv = document.createElement("div"); 
       newDiv.setAttribute("class","radio-item-fecha");
       newDiv.setAttribute("id",divN);
 
       var x = document.createElement("INPUT");
       var y = document.createElement("label");
       x.setAttribute("type", "radio");
       x.setAttribute("name", "sfecha");
       x.setAttribute("value", this.arregloFechPag[i]);
       x.setAttribute("id", id2);
       
       y.setAttribute("for", id2);
       y.setAttribute("id", id2+'l');
       y.innerHTML = this.arregloFechPag[i];
       document.getElementById("radioButtons").appendChild(newDiv); 
       document.getElementById(divN).appendChild(x); 
       document.getElementById(divN).appendChild(y);
   }
 }
*/
  operaCuotas(){
    
    if( this.valRadPorcen == ''){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: '¡Debe seleccionar un porcentaje de pago!'
      })
    }
    else if(this.valRadFecha == ''){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: '¡Debe seleccionar una fecha de pago!'
      })
    }
    else{
      this.cerrarModal2();
      this.mostrarOcultar('pagoPVista1', 'resumenPagIni');
      this.mostrarOcultar('acuerReali', 'resumenPagIni');
      this.valPago = (this.totalDeuda * this.valRadPorcen)/100;
      this.valFaltante = this.totalDeuda - this.valPago;
    }
  }

  mostrarResumen(){
    this.mostrarOcultar('acuerReali','resumenPagIni');
  }

  confirmar(){
      Swal.fire({
        title: 'Acuerdo realizado',
        text: "¿Desea hacer otro acuerdo?",
        icon: 'success',
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonText: `Hacer otro acuerdo`,
        denyButtonText: `Finalizar`,
        buttonsStyling:true
      }).then((result) => {
        if (result.isConfirmed) {
          this.otroAcuerdo('terminosCondiciones','acuerReali','pagoPVista1');
        } else if (result.isDenied) {
          location.reload();
        }
      }) 
  }

 
  otroAcuerdo(ocultar, mostrar1, mostrar2){
    //location.reload();
  
    var divOcultar = document.getElementById(ocultar);
    var divMostrar = document.getElementById(mostrar1);
    var divMostrar2 = document.getElementById(mostrar2);
  
    divOcultar.style.display = "none";
    divMostrar.style.display = "block";
    divMostrar2.style.display = "block";
  }

  reload(){
    window.location.reload();
  }

  telefonos(){
    Swal.fire({
      text: '',
      html:
        '<div style="width: 100%; padding: 9px; background-color: #0d2b49;)">'+
        '<p style="color: #fff; text-align: center;">Comunicate desde cualquier parte del país a uno de los siguientes números, para realizar tu actualización de datos</p>'+
        '</div>'+
        '</div>'+
        '<ul class="list-group list-group-flush">'+
        '<li class="list-group-item"><a href="tel:018000122223" style="text-decoration: none;"><i class="fas fa-phone-alt" style="float: center; color: rgba(0, 82, 148, 1) !important; "></i>'+
        '<br>'+
        '<h3>Linea nacional</h3></li></a>'+
        '<li class="list-group-item"><a href="tel:0318764050" style="text-decoration: none;"><i class="fas fa-phone-alt" style="float: center; color: rgba(0, 82, 148, 1) !important; "></i>'+
        '<br>'+
        '<h3>Desde Bogotá</h3></li></a>'+
        '</ul>',
      showCloseButton: false,
      showCancelButton: false,
      showConfirmButton: false,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Great!',
      confirmButtonAriaLabel: 'Thumbs up, great!',
      cancelButtonText:
        '<i class="fa fa-thumbs-down"></i>',
      cancelButtonAriaLabel: 'Thumbs down'
    })
  }

  puntosPago(){
    Swal.fire({
      text: '',
      html:
        '<div style="width: 100%; padding: 9px; background-color: #0d2b49;)">'+
        '<p style="color: #fff; text-align: center;">Puntos de pago autorizados</p>'+
        '</div>'+
        '<ul class="list-group list-group-flush">'+
        '<li class="list-group-item">'+
        '<div style="width: 40%; float: left; height: 60px; background-image: url(https://s3.amazonaws.com/nequi-wp-colombia/wp-content/uploads/2018/01/facebook.jpg);background-repeat: no-repeat;align-content: center;background-size: contain;)"></div>'+
        '<p style="float: left; margin-left: 5%; margin-top: 3%; text-align: left;">Nequi<br>Pago virtual</p>'+
        '</li>'+
        '<li class="list-group-item">'+
        '<div style="width: 40%; float: left; height: 60px; background-image: url(https://www.daviplata.com/wps/contenthandler/dav/themelist/ThemeDaviPlata/assets/img/logo-daviplata.png);background-repeat: no-repeat;align-content: center;background-size: contain;)"></div>'+
        '<p style="float: left; margin-left: 5%; margin-top: 3%; text-align: left;">Daviplata<br>Pago virtual</p>'+
        '</li>'+
        '<li class="list-group-item">'+
        '<div style="width: 40%; float: left; height: 60px; background-image: url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlhIRHrVksajhbqdOGDnU27MzL7QWBXZ9U_Q&usqp=CAU);background-repeat: no-repeat;align-content: center;background-size: contain;)"></div>'+
        '<p style="float: left; margin-left: 5%; margin-top: 3%; text-align: left;">PSE<br>Pago virtual</p>'+
        '</li>'+
        '<li class="list-group-item">'+
        '<p style="text-align: left; font-size: 13px">Otros medios:<br>'+
        '* Puntos de Recaudo Efecty. Convenio 110062<br>'+
        '* Puntos de Pago Baloto002E<br>'+
        '* Banco Davivienda - CONVENIO 1008499<br>'+
        '* Banco de Bogotá - Cuenta Corriente No. 095746970<br>'+
        '* Bancolombia - CONVENIO PATE 082.</p><br>'+
        '</li>'+
        '</ul>',
      showCloseButton: false,
      showCancelButton: false,
      showConfirmButton: false,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Great!',
      confirmButtonAriaLabel: 'Thumbs up, great!',
      cancelButtonText:
        '<i class="fa fa-thumbs-down"></i>',
      cancelButtonAriaLabel: 'Thumbs down'
    })
  }


}
