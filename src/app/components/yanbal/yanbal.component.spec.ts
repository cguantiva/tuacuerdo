import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YanbalComponent } from './yanbal.component';

describe('YanbalComponent', () => {
  let component: YanbalComponent;
  let fixture: ComponentFixture<YanbalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YanbalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YanbalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
