import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConsultaInfoService } from 'src/app/services/consulta-info.service';
import {ValidarService} from '../../services/validar.service';

@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.css']
})
export class RedirectComponent implements OnInit {
  token: string;
  data: any = {};

  constructor(private route: ActivatedRoute, private router: Router, private validarService: ValidarService, private consultaInfoService: ConsultaInfoService) { }
  

  ngOnInit(): void {

    this.route.paramMap.subscribe(params =>{
      if(params.has("token")){
        this.token = params.get ("token")
        //console.log("Parametros de url: ", this.token);
        localStorage.setItem('token', this.token);
        //this.router.navigate(['/fundacion']);
        console.log("Prueba URL: ", this.token);

        this.consultaInfoService.infoInicial(this.token).subscribe(
          res => {
            this.data = res;
            localStorage.setItem('negociacion_agrupada', this.data.infoInicial[0].negociacion_agrupada);
            localStorage.setItem('nombreLog', this.data.infoInicial[0].nombresApellidos);
            localStorage.setItem('docuLog', this.data.infoInicial[0].identificacion);
            
            
            if(this.data.infoInicial == ''){
              this.validarService.loggedOut();
            }else if(this.data.infoInicial[0].empresa_cobranza == 'fundamujer'){
              this.router.navigate(['/fundacion']);
            }
          },
          err =>{
            this.validarService.loggedOut();
            console.log(err);
          }
        );
        
       /* this.consultaInfoService.consultaInfo(this.token).subscribe(
          res =>{
            console.log(res);
            this.data = res;
            console.log("mockup: ", this.data.tipoMockups);
            
            if(this.data.canOfertas == '0'){
              this.validarService.loggedOut();
              //this.router.navigate(['/fundacion']);
            }else{
              if(this.data.canOfertas == '1'){
                this.router.navigate(['/juriscoop']);
              }
              else if(this.data.canOfertas == '2'){
                this.router.navigate(['/fundacion']);
              }else{
                this.validarService.loggedOut();
                
              }
            }
          },
          err =>{
            console.log(err);
            
          }
        );*/
        
      }else{
        console.log("HAY UN ERROR");
      }
    })
  }

}
