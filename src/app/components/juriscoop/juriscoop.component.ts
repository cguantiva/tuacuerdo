import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import 'sweetalert2/src/sweetalert2.scss';
import {ConsultaInfoService} from '../../services/consulta-info.service';

@Component({
  selector: 'app-juriscoop',
  templateUrl: './juriscoop.component.html',
  styleUrls: ['./juriscoop.component.css']
})
export class JuriscoopComponent implements OnInit {
 

  idVer: any = 1;
  idOcultar: any = 1;
  data: any = {};
  datos: any [];
  nomUsu: any;
  valSaldoTotal: any;
  numCuotas: any;
  nombImgSoporte: any;
  token: any;
  flagBtnEnvFot: boolean = false;

  
  constructor(private consultaInfoService: ConsultaInfoService) { }

  ngOnInit(): void {

    if(document.getElementById("btnModal")){
      const modal = document.getElementById("comPago");
      const btn = document.getElementById("btnModal");
      const span = document.getElementsByClassName("close")[0];
      const body = document.getElementsByTagName("body")[0];

      btn.onclick = function() {
        modal.style.display = "block";
        body.style.position = "static";
        body.style.height = "100%";
        body.style.overflow = "hidden";
      }
      window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none";

          body.style.position = "inherit";
          body.style.height = "auto";
          body.style.overflow = "visible";
        }
      }
    }

    this.token = localStorage.getItem('token');

    this.consultaInfoService.infoInicial(this.token).subscribe(
      res =>{
        console.log("completo: ",res);
        this.data = res;
        this.datos = this.data.resulOfertas;
        this.nomUsu = this.datos[0].nombres_persona;
        this.valSaldoTotal = this.datos[0].valor_original_saldo_total;
        this.numCuotas = this.datos[0].numero_cuotas;
        //console.log("saldoTotal: ", this.valSaldoTotal );
      },
      err =>{
        console.log(err);
        
      }
    )
  }


  mostrarOcultar(ocultar, mostrar){
    var divOcultar = document.getElementById(ocultar);
    var divMostrar = document.getElementById(mostrar);
  
    divOcultar.style.display = "none";
    divMostrar.style.display = "block";
  }
 
  pago(){
    //this.cerrarModal();
    Swal.fire({
      imageUrl: '../../assets/imagenes/1.png',
      imageWidth: 350,
      imageHeight: 400,
      imageAlt: 'Custom image',
    })
  }

  public cargandoImagen(files: FileList){
    if(files){
     this.flagBtnEnvFot = true;
    }

    const formData: FormData = new FormData();

    console.log("image: ", files[0]);
    this.nombImgSoporte = this.datos[0].identificacion_persona + files[0].name;
    console.log("Nombre imagen: ", this.nombImgSoporte);
  
  }

  motivNoAcept(motiv: any){
    Swal.fire(
      'Gracias' + motiv,
      'Te esperamos pronto',
      'success'
    )
  }

  cerrarModal(){
    if(document.getElementById("comPago")){
      const modal = document.getElementById("tvesModal");
      const btn = document.getElementById("btnModal");
      const span = document.getElementsByClassName("close")[0];
      const body = document.getElementsByTagName("body")[0];

          modal.style.display = "none";
          body.style.position = "inherit";
          body.style.height = "auto";
          body.style.overflow = "visible";     
    }
  }

  contacto(){
    //this.cerrarModal();
    Swal.fire(
      'Contactanos',
      'Si tienes inquietudes puedes comunicarte a la linea 018000111464',
      'info'
    )
  }

  

}
