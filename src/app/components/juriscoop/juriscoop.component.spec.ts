import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JuriscoopComponent } from './juriscoop.component';

describe('JuriscoopComponent', () => {
  let component: JuriscoopComponent;
  let fixture: ComponentFixture<JuriscoopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JuriscoopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JuriscoopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
