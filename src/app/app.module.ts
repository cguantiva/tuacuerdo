import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule , ReactiveFormsModule } from '@angular/forms' ; 
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxWhastappButtonModule } from "ngx-whatsapp-button";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './auth.guard';
import { InicioComponent } from './components/inicio/inicio.component';
import { RedirectComponent } from './components/redirect/redirect.component';
import { JuriscoopComponent } from './components/juriscoop/juriscoop.component';
import { YanbalComponent } from './components/yanbal/yanbal.component';
import { from } from 'rxjs';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FundaMujerComponent } from './components/funda-mujer/funda-mujer.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    RedirectComponent,
    JuriscoopComponent,
    YanbalComponent,
    FundaMujerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule ,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    BrowserAnimationsModule,
    NgxWhastappButtonModule
  ],
  providers: [AuthGuard,HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
