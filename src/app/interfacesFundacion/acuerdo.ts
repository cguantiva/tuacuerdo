export interface Acuerdo {
    idOferta: string;
    valor_acuerdo: string;
    valor_cuota : string;
    fecha_pago: string;
    numero_cuotas: string;
    token: string;
    punto_pago: string;
}
