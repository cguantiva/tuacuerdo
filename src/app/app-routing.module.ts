import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InicioComponent} from './components/inicio/inicio.component';
import {RedirectComponent} from './components/redirect/redirect.component';
import {JuriscoopComponent} from './components/juriscoop/juriscoop.component';
import {YanbalComponent} from './components/yanbal/yanbal.component';
import {FundaMujerComponent} from './components/funda-mujer/funda-mujer.component';
import {AuthGuard} from './auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'inicio',
    pathMatch: 'full'
  },
  {
    path: 'inicio',
    component: InicioComponent,
  },
  {
    path: 'redirect/:token',
    component: RedirectComponent,
  },
  {
    path: 'juriscoop',
    component: JuriscoopComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'yanbal',
    component: YanbalComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'fundacion',
    component: FundaMujerComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
